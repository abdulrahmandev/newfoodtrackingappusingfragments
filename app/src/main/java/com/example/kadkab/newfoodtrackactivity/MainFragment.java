package com.example.kadkab.newfoodtrackactivity;

import android.animation.LayoutTransition;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.List;

/**
 * Created by kadkab on 1/1/2016.
 */
public class MainFragment extends Fragment {

    private FragmentManager fragmentManager;
    public static ListView notExpandedList;
    public static Fragment mainContentFragment;
    public static LinearLayout layout;
    public static FrameLayout expandedContainer;

    public  MainFragment(FragmentManager mFragmentManager){
        super();
        this.fragmentManager = mFragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        layout = (LinearLayout)inflater.inflate(R.layout.fragment_not_expanded, container, false);

        notExpandedList = (ListView) layout.findViewById(R.id.not_expanded_list);

        Fragment expandedFragment = new ExpandedFragment(this.fragmentManager);
        this.fragmentManager.beginTransaction()
                .add(R.id.expanded_fragment_container,expandedFragment , "main")
                .disallowAddToBackStack().commit();

        expandedContainer = (FrameLayout) layout.findViewById(R.id.expanded_fragment_container);
//        float graphWidth = getResources().getDrawable(R.drawable.graph).getIntrinsicHeight();
//        expandedContainer.animate().y(graphWidth);


        //Todo main_content add fragment to this Id.
//        mainContentFragment = new MainProfileLayout();
//        this.fragmentManager.beginTransaction()
//                .add(R.id.main_content,mainContentFragment , "main").commit();

//        RelativeLayout relativeLayout = (RelativeLayout) layout.findViewById(R.id.next_fragment_trigger);
//        relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                layout.setVisibility(View.GONE);
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_top);
//                Fragment expandedFragment = new ExpandedFragment(fragmentManager);
//                fragmentTransaction.replace(R.id.fragment_container, expandedFragment);
//                fragmentTransaction.commit();
//            }
//        });

        return layout;

    }
}
