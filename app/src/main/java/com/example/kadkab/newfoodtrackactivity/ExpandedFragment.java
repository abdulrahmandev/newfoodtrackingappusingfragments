package com.example.kadkab.newfoodtrackactivity;

import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by kadkab on 1/1/2016.
 */
public class ExpandedFragment extends Fragment {

    private float graphWidth;
    private FragmentManager fragmentManager;

    public ExpandedFragment(FragmentManager mFragmentManager){
        this.fragmentManager = mFragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        graphWidth = getResources().getDrawable(R.drawable.graph).getIntrinsicHeight();
//        ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f);
//        scale.setInterpolator(new LinearInterpolator());

        final LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.fragment_expanded, container, false);


//        fragmentTransaction.commit();

        RelativeLayout relLayout = (RelativeLayout) layout.findViewById(R.id.cal_burnt_in_expanded);
        relLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutTransition l = new LayoutTransition();
                l.enableTransitionType(LayoutTransition.DISAPPEARING);

                ObjectAnimator objectAnimator= ObjectAnimator.ofFloat(MainFragment.expandedContainer, "translationY", graphWidth);
                objectAnimator.setDuration(1000);
                // change the visibility to gone.
                l.setAnimator(LayoutTransition.CHANGING,objectAnimator);
                l.setAnimator(LayoutTransition.CHANGE_APPEARING,objectAnimator);
                l.setAnimator(LayoutTransition.CHANGE_DISAPPEARING,objectAnimator);
                l.setAnimator(LayoutTransition.APPEARING,objectAnimator);
                l.setAnimator(LayoutTransition.DISAPPEARING,objectAnimator);
                MainFragment.layout.setLayoutTransition(l);
//                objectAnimator.start();
                MainFragment.notExpandedList.setVisibility(View.GONE);
                // TODO: 1/2/2016  remove fragment from main_content -- not working

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.setCustomAnimations( R.anim.slide_out_top,R.anim.slide_in_bottom );
//
//                fragmentManager.beginTransaction().remove(MainFragment.mainContentFragment).commit();

                fragmentTransaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_top);

                Fragment graphFragment = new GraphFragment();
                fragmentTransaction.replace(R.id.graph_container, graphFragment);

                Fragment microNutrientFragment = new MicroNutrientFragment();
                fragmentTransaction.replace(R.id.micro_nutirents_container, microNutrientFragment);

                Fragment nutrientFragment = new NutrientFragment();
                fragmentTransaction.replace(R.id.nutirents_container, nutrientFragment);
                fragmentTransaction.commit();
            }
        });
//        Fragment calBurntFragment = new CalBurntFragment();
//        fragmentTransaction.replace(R.id.cal_burnt_container, calBurntFragment);
//        fragmentTransaction.commit();

//        fragmentTransaction.commit();


//        ImageView graphView = (ImageView) layout.findViewById(R.id.graph);
//        graphView.setVisibility(View.VISIBLE);
//        graphView.animate().translationY(0).withLayer();
////        graphView.setAnimation(scale);
//
//        RelativeLayout calBurnt = (RelativeLayout) layout.findViewById(R.id.cal_burnt_in_expanded);
//        calBurnt.setVisibility(View.VISIBLE);
////        calBurnt.setAnimation(scale);
//
//        TextView microNutrient = (TextView) layout.findViewById(R.id.macro_nutrient);
//        microNutrient.setVisibility(View.VISIBLE);
////        microNutrient.setAnimation(scale);
//
//        LinearLayout nutrient = (LinearLayout) layout.findViewById(R.id.nutrient);
//        nutrient.setVisibility(View.VISIBLE);
////        nutrient.setAnimation(scale);

//        layout.setLayoutTransition(l);

        return layout;
    }
}
